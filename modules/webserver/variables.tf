variable "name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_sg" {
  type = any
}

variable "key_name" {
  type = string
}