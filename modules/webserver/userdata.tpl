#!/bin/bash
sudo apt update -y &&
sudo apt install nginx -y
sudo apt install npm -y
sudo npm install pm2 -g
sudo systemctl start nginx
sudo systemctl enable nginx
sudo systemctl status nginx
mkdir /home/ubuntu/todolist-backend
sudo chown ubuntu /var/www/html
sudo apt install docker.io -y
sudo apt install mysql-client-core-8.0 -y